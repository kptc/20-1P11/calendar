﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ClassLibrary;

namespace UnitTest
{
    /// <summary>
    /// Представление и цвет
    /// </summary>
    [TestClass]
    public class VC
    {
        [TestMethod]
        public void VC_1()
        {
            Application app = Application.GetApp();
            app.Settings.Color = Application.Color.White;
            app.Settings.View = Application.View.Compact;
            Assert.AreEqual(Application.Color.White, app.Settings.Color, "Не удалось обновить цвет");
            Assert.AreEqual(Application.View.Compact, app.Settings.View, "Не удалось обновить представление");
        }

        [TestMethod]
        public void VC_2()
        {
            Application app = Application.GetApp();
            app.Settings.Color = Application.Color.Black;
            app.Settings.View = Application.View.Adaptive;
            Assert.AreEqual(Application.Color.Black, app.Settings.Color, "Не удалось обновить цвет");
            Assert.AreEqual(Application.View.Adaptive, app.Settings.View, "Не удалось обновить представление");
        }
    }
}
